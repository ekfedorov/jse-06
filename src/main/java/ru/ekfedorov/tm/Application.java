package ru.ekfedorov.tm;

import ru.ekfedorov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        displayWelcome();
        if (run(args)) exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_HELP: displayHelp(); break;
            case TerminalConst.CMD_VERSION: displayVersion(); break;
            case TerminalConst.CMD_ABOUT: displayAbout(); break;
            case TerminalConst.CMD_EXIT: exit(); break;
            default: showIncorrectCommand();
        }
    }

    private static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    private static boolean run(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        parseArg(param);
        return true;
    }

    private static void displayHelp() {
        System.out.println(TerminalConst.CMD_VERSION + " - Display program version.");
        System.out.println(TerminalConst.CMD_ABOUT + " - Display developer info.");
        System.out.println(TerminalConst.CMD_HELP + " - Display list of terminal commands.");
        System.out.println(TerminalConst.CMD_EXIT + " - Close application.");
    }

    private static void displayVersion() {
        System.out.println("1.1.0");
    }

    private static void displayAbout() {
        System.out.println("Evgeniy Fedorov");
        System.out.println("ekfedorov@tsconsulting.com");
    }

    private static void exit() {
        System.exit(0);
    }

}
